//-----------------------------------------------------------------------------
/// build flags
///

/// print debug messages if defined
#define DEBUG_PRINTS

//-----------------------------------------------------------------------------
/// macro for printing debug messages
#ifdef DEBUG_PRINTS
#define PRINTD(...) Serial.println(__VA_ARGS__)
#else
#define PRINTD(...)
#endif

// #define CLOCK_KITCHEN
#define CLOCK_LIVING_ROOM

#include <EEPROM.h>

#include <SPI.h>
#include "RF24.h"
#include <avr/wdt.h>

//-----------------------------------------------------------------------------
/// SENSORS begin
#define SENSOR_TEMP  A1
#define SENSOR_LIGHT A0

uint8_t getLightPercent();
uint16_t getTemperatureMilliCelsius();
/// SENSORS end
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/// RADIO begin

/// the nRF24L01 is connected via SPI. Pin CSN is the chip-select line.
#define CE_PIN  9
#define CS_PIN 10

/// The IRQ pin of the nRF24L01
#define RF24_IRQ_PIN 2

/// Channel number of the NRF24.
/// The center frequency is 2400 MHz + RF24_CHANNEL_NUMBER. {1...125}
#define RF24_CHANNEL_NUMBER 100

#define RXBUF_LEN 7
#define TXBUF_LEN 5

#define CMD_CODE_ACK            0xAC

#define CMD_CODE_TIME_SYN       0x80
#define CMD_CODE_LED_TOGGLE     0x81
#define CMD_CODE_CHANNEL_CHANGE 0x82
#define CMD_CODE_PA_CHANGE      0x83
#define CMD_CODE_LIGHT_SENS     0x84
#define CMD_CODE_TEMP_SENS      0x85
#define CMD_CODE_SET_LEDS       0x86
#define CMD_CODE_TIME           0x87
#define CMD_CODE_CALIB_TEMP     0x88
#define CMD_CODE_CALIB_LIGHT    0x89

#define REQ_CODE_TIME_SYN       0x40

#define PERIODIC_SENSOR_MEAS    0xAA

#define SENSOR_CALIB_EEPROM_ADDR 0x10

/// struct to receive time updates
typedef struct {
  uint8_t h;
  uint8_t m;
  uint8_t s;
  uint8_t h_syn;
  uint8_t m_syn;
  uint8_t s_syn;
} time_packet_t;

/// structure to send measured sensor values
typedef struct {
  uint8_t sequence_number;
  uint8_t light;
  uint16_t temperature;
} measurements_packet_t;

/// structure to set leds to specific values
typedef struct {
  uint8_t h;
  uint8_t m;
  uint8_t s;
} set_leds_packet_t;

/// Radio instance
RF24 radio(CE_PIN, CS_PIN);

/// buffer to receive seqence number, hh:mm:ss time, hh:mm:ss nextsync time
uint8_t rx_buf_[RXBUF_LEN];

/// buffer to send sensor data
uint8_t tx_buf_[TXBUF_LEN];

time_packet_t tp_;

measurements_packet_t mp_;

/// flag to control if time update has been received
volatile bool time_updated_;

/// true, if radio is active, false otherwise
bool radioIsOn_;

bool retransmit_;

/// addresses of sender and receiver
#ifdef CLOCK_KITCHEN
const uint64_t address_w[1] = {0xA0A1A2A3A4LL};
const uint64_t address_r[1] = {0x50515253A4LL};
#endif

#ifdef CLOCK_LIVING_ROOM
const uint64_t address_w[1] = {0xA0A1A2A3B5LL};
const uint64_t address_r[1] = {0x50515253B5LL};
#endif


void (*setLedsCb_)(void);
bool leds_toggle_state_;
rf24_pa_dbm_e current_pa_;
uint8_t current_channel_;
uint32_t any_leds_;
uint8_t state_change_timeout_; // in seconds

bool update_calib_data_;
struct calib_data_t {
  uint8_t leds_off_threshold_percent;
  uint16_t calib_temperature_mdeg;
  uint16_t calib_temperature_int;
};
struct calib_data_t calib_data_;

//-----------------------------------------------------------------------------
/// @function initializeRadio
///
/// called during setup()
/// blocks execution until a time stamp has been received
bool initializeRadio();

//-----------------------------------------------------------------------------
/// @function resetTxBuf
///
/// set buffer memory to 0
void resetTxBuf();

//-----------------------------------------------------------------------------
/// @function resetRxBuf
///
/// set buffer memory to 0
void resetRxBuf();

//-----------------------------------------------------------------------------
/// @function rf24_isr
///
/// called if a packet has been received
void rf24_isr();

//-----------------------------------------------------------------------------
/// @function activateRF24
///
/// switch on and configure radio
void activateRF24();

//-----------------------------------------------------------------------------
/// @function deactivateRF24
///
/// switch off radio and disable SPI bus.
/// This is necessary to set the LEDs
void deactivateRF24();

//-----------------------------------------------------------------------------
/// @function sendMeasurements
///
/// send mp_ content via radio
void sendMeasurements();

//-----------------------------------------------------------------------------
/// @function syncTime
///
/// receive the current time to correct clock drift and store the point in
/// time for next synchronization.
void syncTime();

//-----------------------------------------------------------------------------
/// @function send
///
/// transmit the tx_buf_ buffer
/// @param max_retries Maximum number of retries, if transmission failed
/// @param retry_delay Time between retries in milliseconds
/// @return true, if sending was successful before transmitting max_retries
///         packets.
bool send(uint8_t max_retries, uint32_t retry_delay);

/// RADIO end
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
/// pin definitions

/// The pins CLK0, CLK1 and CLK2 drive the clock lines of the hex flipflop ICs
#define CLK0 5
#define CLK1 6
#define CLK2 7

/// The pins OUT0..OUT5 are parallel outputs connected to the hex flipflop
/// data inputs. They are read by the flipflops at every positive edge of the
/// clock signal.
#define OUT0 8
#define OUT1 9
#define OUT2 4
#define OUT3 11
#define OUT4 12
#define OUT5 13

/// There is one error LED connected to pin ERRLED
#define ERRLED 3

/// These 3 variables are used by setLeds() to switch the leds on or off.
/// They are volatile, because they are used inside an ISR.
/// Each variable is used to set one row. h_, m_, s_ control top, middle, bottom
/// @see rf24_isr()
volatile unsigned char s_;
volatile unsigned char m_;
volatile unsigned char h_;

/// these two variable are used to measure the time.
unsigned long current_;
unsigned long next_;

//-----------------------------------------------------------------------------
/// @function setLeds
///
/// initialize the leds according to the h_, m_, s_ variables
void setLeds();

//-----------------------------------------------------------------------------
/// @function allLedsOn
///
/// used during startup for debugging
void allLedsOn();

//-----------------------------------------------------------------------------
/// @function blink
///
/// switch all leds on and off 'x' times with delay 'y'
/// @param x Repetions on ON/OFF
/// @param y delay between ON and OFF in milliseconds
void blink(int x, int y);

void ledsToggle();

void ledsTemperature();
void ledsLight();
void ledsAny();
void listenForCmd(uint32_t timeout);

void updateEeprom();

//-----------------------------------------------------------------------------
/// @function setup
///
/// This function is executed once after booting.
/// All globarl variables are initialized with their default values.
/// If INC_BTLE is defined, the program will wait for a time broadcast on one
/// of the BTLE broadcast channels.
void setup() {
#ifdef DEBUG_PRINTS
  Serial.begin(115200);
  while (!Serial) { }
  Serial.print(F("\n\nCompiled on "));
  Serial.print(__DATE__);
  Serial.print(" at ");
  Serial.print(__TIME__);
  Serial.println("\n");
#endif

  // default functionality:
  setLedsCb_ = setLeds;

  leds_toggle_state_ = HIGH;
  current_pa_ = RF24_PA_MAX;
  current_channel_ = RF24_CHANNEL_NUMBER;
  retransmit_ = true;
  state_change_timeout_ = 255;

  update_calib_data_ = false;
  EEPROM.get(SENSOR_CALIB_EEPROM_ADDR, calib_data_);
  if (calib_data_.calib_temperature_int == 0      ||
      calib_data_.calib_temperature_int > 50      ||
      calib_data_.leds_off_threshold_percent == 0 ||
      calib_data_.leds_off_threshold_percent > 100  ) {
    calib_data_.calib_temperature_mdeg = 27000;
    calib_data_.calib_temperature_int = 537;
    calib_data_.leds_off_threshold_percent = 20;
    update_calib_data_ = true;
  }
  PRINTD(F("calib_data_"));
  PRINTD(calib_data_.calib_temperature_mdeg);
  PRINTD(calib_data_.calib_temperature_int);
  PRINTD(calib_data_.leds_off_threshold_percent);

  mp_.sequence_number = 1;
  mp_.light = getLightPercent();
  mp_.temperature = getTemperatureMilliCelsius();

  // testing all leds
  allLedsOn();
  
  blink(10, 100);
  blink(20, 50);
  blink(150, 10);

  h_ = 0;
  m_ = 0;
  s_ = 0;
  next_ = 0;
  current_ = 0;
  setLeds();

  digitalWrite(ERRLED, HIGH);
  if(initializeRadio()) {
    digitalWrite(ERRLED, LOW);
  } else {
    PRINTD(F("ERROR initializing radio"));
    while(true);
  }
  wdt_enable(WDTO_4S);
  PRINTD(F("requesting time update"));
  resetTxBuf();
  resetRxBuf();
  time_updated_ = false;
  tx_buf_[0] = REQ_CODE_TIME_SYN;
  while(!time_updated_ && retransmit_) {
    radio.stopListening();
    wdt_reset();
    radio.write(tx_buf_, TXBUF_LEN);
    radio.startListening();
    delay(500);
  }
  while (!time_updated_) {
    delay(100);
  }
  wdt_disable();
  deactivateRF24();
}

//-----------------------------------------------------------------------------
/// @function loop
///
/// This is the main function and is executed forever. It checks the
/// milliseconds counter and increases the global variables holding the
/// values for hours, minutes and seconds.
/// After updating the variables, the function setLeds() updates all 3 hex
/// flipflops.
void loop() {
  // if one second has passed
  current_ = millis();
  if (current_ > next_) {
    next_ = current_ + 1000UL;
    s_++;
    if (state_change_timeout_ == 0 && state_change_timeout_ != 255) {
      PRINTD(F("state_to"));
      setLedsCb_ = setLeds;
      state_change_timeout_ = 255;
    } else {
      state_change_timeout_--;
    }
    // if (!(s_ % 5)) {
      listenForCmd(200);
    // }
    if (s_ > 59) {
      s_ = 0;
      m_++;
#ifdef CLOCK_KITCHEN
      if ((m_ % 2) == 0) {
#endif
#ifdef CLOCK_LIVING_ROOM
      if ((m_ % 2) == 1) {
#endif
        sendMeasurements();
      }
      if (mp_.light < calib_data_.leds_off_threshold_percent) {
        PRINTD(F("off"));
        leds_toggle_state_ = false;
        setLedsCb_ = ledsToggle;
      } else {
        PRINTD(F("on"));
        setLedsCb_ = setLeds;
      }
      PRINTD(mp_.light);
      if (h_ == tp_.h_syn && m_ == tp_.m_syn) {
        syncTime();
      }
      if (m_ > 59) {
        m_ = 0;
        h_++;
        if (h_ > 23) {
          h_ = 0;
        }
        if (h_ == 4) {
          // maintainance
          updateEeprom();
        }
      }
    }
  }
  setLedsCb_();
}

//-----------------------------------------------------------------------------
#ifdef CLOCK_KITCHEN
void setLeds() {
  // seconds
  digitalWrite(OUT0, s_ & 0x1);
  digitalWrite(OUT1, s_ & 0x2);
  digitalWrite(OUT2, s_ & 0x4);
  digitalWrite(OUT3, s_ & 0x8);
  digitalWrite(OUT4, s_ & 0x10);
  digitalWrite(OUT5, s_ & 0x20);
  digitalWrite(CLK0, 1);
  digitalWrite(CLK0, 0);

  // minutes
  digitalWrite(OUT0, m_ & 0x2);
  digitalWrite(OUT1, m_ & 0x1);
  digitalWrite(OUT2, m_ & 0x4);
  digitalWrite(OUT3, m_ & 0x8);
  digitalWrite(OUT4, m_ & 0x10);
  digitalWrite(OUT5, m_ & 0x20);
  digitalWrite(CLK1, 1);
  digitalWrite(CLK1, 0);

  // hours
  digitalWrite(OUT0, h_ & 0x1);
  digitalWrite(OUT1, h_ & 0x2);
  digitalWrite(OUT2, h_ & 0x4);
  digitalWrite(OUT3, h_ & 0x8);
  digitalWrite(OUT4, h_ & 0x10);
  digitalWrite(OUT5, h_ & 0x20);
  digitalWrite(CLK2, 1);
  digitalWrite(CLK2, 0);
}
#endif
#ifdef CLOCK_LIVING_ROOM
void setLeds() {
  // seconds
  digitalWrite(OUT5, s_ & 0x1);
  digitalWrite(OUT4, s_ & 0x2);
  digitalWrite(OUT3, s_ & 0x4);
  digitalWrite(OUT2, s_ & 0x8);
  digitalWrite(OUT1, s_ & 0x10);
  digitalWrite(OUT0, s_ & 0x20);
  digitalWrite(CLK2, 1);
  digitalWrite(CLK2, 0);

  // minutes
  digitalWrite(OUT5, m_ & 0x2);
  digitalWrite(OUT4, m_ & 0x1);
  digitalWrite(OUT3, m_ & 0x4);
  digitalWrite(OUT2, m_ & 0x8);
  digitalWrite(OUT1, m_ & 0x10);
  digitalWrite(OUT0, m_ & 0x20);
  digitalWrite(CLK1, 1);
  digitalWrite(CLK1, 0);

  // hours
  digitalWrite(OUT5, h_ & 0x1);
  digitalWrite(OUT4, h_ & 0x2);
  digitalWrite(OUT3, h_ & 0x4);
  digitalWrite(OUT2, h_ & 0x8);
  digitalWrite(OUT1, h_ & 0x10);
  digitalWrite(OUT0, h_ & 0x20);
  digitalWrite(CLK0, 1);
  digitalWrite(CLK0, 0);
}
#endif

//-----------------------------------------------------------------------------
void allLedsOn() {
  pinMode(ERRLED, OUTPUT);

  pinMode(CLK0, OUTPUT);
  pinMode(CLK1, OUTPUT);
  pinMode(CLK2, OUTPUT);

  pinMode(OUT0, OUTPUT);
  pinMode(OUT1, OUTPUT);
  pinMode(OUT2, OUTPUT);
  pinMode(OUT3, OUTPUT);
  pinMode(OUT4, OUTPUT);
  pinMode(OUT5, OUTPUT);

  // switch on error led
  digitalWrite(ERRLED, HIGH);

  // all output to HIGH
  digitalWrite(OUT0, HIGH);
  digitalWrite(OUT1, HIGH);
  digitalWrite(OUT2, HIGH);
  digitalWrite(OUT3, HIGH);
  digitalWrite(OUT4, HIGH);
  digitalWrite(OUT5, HIGH);

  // trigger clocks of flip-flops
  delay(1000);
  digitalWrite(CLK0, HIGH);
  digitalWrite(CLK1, HIGH);
  digitalWrite(CLK2, HIGH);

  delay(1000);
  digitalWrite(CLK0, LOW);
  digitalWrite(CLK1, LOW);
  digitalWrite(CLK2, LOW);

  // switch off error led again
  digitalWrite(ERRLED, LOW);
}

//-----------------------------------------------------------------------------
void blink(int x, int y) {
  for (int i = 0; i < x; i++) {
    delay(y);
    s_ = 63;
    m_ = 63;
    h_ = 63;
    setLeds();
    delay(y);
    s_ = 0;
    m_ = 0;
    h_ = 0;
    setLeds();
  }
  delay(y);
  m_ = 0;
  h_ = 0;
  delay(y);
  s_ = 0x3f;
  setLeds();
  delay(y);
  s_ = 0;
  delay(y);
  s_ = 0;
  h_ = 0;
  delay(y);
  m_ = 0x3f;
  setLeds();
  delay(y);
  m_ = 0;
  delay(y);
  m_ = 0;
  s_ = 0;
  delay(y);
  h_ = 0x3f;
  setLeds();
  delay(y);
  h_ = 0;
  delay(y);
}

//-----------------------------------------------------------------------------
void ledsToggle() {
  digitalWrite(OUT0, leds_toggle_state_);
  digitalWrite(OUT1, leds_toggle_state_);
  digitalWrite(OUT2, leds_toggle_state_);
  digitalWrite(OUT3, leds_toggle_state_);
  digitalWrite(OUT4, leds_toggle_state_);
  digitalWrite(OUT5, leds_toggle_state_);
    // seconds
  digitalWrite(CLK0, 1);
  digitalWrite(CLK0, 0);
  // minutes
  digitalWrite(CLK1, 1);
  digitalWrite(CLK1, 0);
  // hours
  digitalWrite(CLK2, 1);
  digitalWrite(CLK2, 0);
}

//-----------------------------------------------------------------------------
uint8_t getLightPercent() {
  uint16_t val = analogRead(SENSOR_LIGHT);
  uint8_t perc = map(val, 0, 1023, 0, 100);
  PRINTD(F("lightval"));
  PRINTD(perc);
  return perc;
}

//-----------------------------------------------------------------------------
uint16_t getTemperatureMilliCelsius() {
  // return temperature in milli-degree celsius
  // 2.982 V (= 610.117 ADC) @ 25deg
  // 10mV/K change. 1U_LSB = 4.89mV
  uint16_t t = analogRead(SENSOR_TEMP);
  PRINTD(F("gettemp"));
  PRINTD(t);
  // u_lsb = 5V / 1024 = 0.0048828125
  // (int_calib - int_meas) * u_lsb / 10mV + deg_calib
  uint16_t tmp = (uint16_t)(((float)calib_data_.calib_temperature_int - (float)t) * 488.28125f);
  tmp += calib_data_.calib_temperature_mdeg;
  PRINTD(tmp);
  return tmp;
}

//-----------------------------------------------------------------------------
void ledsTemperature() {
  uint8_t set = (getTemperatureMilliCelsius() >> 8);
  PRINTD(set);
  // seconds
  digitalWrite(OUT0, set & 0x1);
  digitalWrite(OUT1, set & 0x2);
  digitalWrite(OUT2, set & 0x4);
  digitalWrite(OUT3, set & 0x8);
  digitalWrite(OUT4, set & 0x10);
  digitalWrite(OUT5, set & 0x20);
  digitalWrite(CLK0, 1);
  digitalWrite(CLK0, 0);

  // minutes
  digitalWrite(OUT0, set & 0x2);
  digitalWrite(OUT1, set & 0x1);
  digitalWrite(OUT2, set & 0x4);
  digitalWrite(OUT3, set & 0x8);
  digitalWrite(OUT4, set & 0x10);
  digitalWrite(OUT5, set & 0x20);
  digitalWrite(CLK1, 1);
  digitalWrite(CLK1, 0);

  // hours
  digitalWrite(OUT0, set & 0x1);
  digitalWrite(OUT1, set & 0x2);
  digitalWrite(OUT2, set & 0x4);
  digitalWrite(OUT3, set & 0x8);
  digitalWrite(OUT4, set & 0x10);
  digitalWrite(OUT5, set & 0x20);
  digitalWrite(CLK2, 1);
  digitalWrite(CLK2, 0);
}

//-----------------------------------------------------------------------------
void ledsLight() {
  uint16_t val = analogRead(SENSOR_LIGHT);
  uint8_t set = map(val, 0, 1023, 0, 63);
  digitalWrite(OUT0, set & 0x01);
  digitalWrite(OUT1, set & 0x02);
  digitalWrite(OUT2, set & 0x04);
  digitalWrite(OUT3, set & 0x08);
  digitalWrite(OUT4, set & 0x10);
  digitalWrite(OUT5, set & 0x20);
    // seconds
  digitalWrite(CLK0, 1);
  digitalWrite(CLK0, 0);
  // minutes
  digitalWrite(CLK1, 1);
  digitalWrite(CLK1, 0);
  // hours
  digitalWrite(CLK2, 1);
  digitalWrite(CLK2, 0);
}

//-----------------------------------------------------------------------------
void ledsAny() {
  // seconds
  digitalWrite(OUT0, any_leds_ & 0x01);
  digitalWrite(OUT1, any_leds_ & 0x02);
  digitalWrite(OUT2, any_leds_ & 0x04);
  digitalWrite(OUT3, any_leds_ & 0x08);
  digitalWrite(OUT4, any_leds_ & 0x10);
  digitalWrite(OUT5, any_leds_ & 0x20);
  digitalWrite(CLK0, 1);
  digitalWrite(CLK0, 0);

  // minutes
  digitalWrite(OUT0, any_leds_ & 0x0200);
  digitalWrite(OUT1, any_leds_ & 0x0100);
  digitalWrite(OUT2, any_leds_ & 0x0400);
  digitalWrite(OUT3, any_leds_ & 0x0800);
  digitalWrite(OUT4, any_leds_ & 0x1000);
  digitalWrite(OUT5, any_leds_ & 0x2000);
  digitalWrite(CLK1, 1);
  digitalWrite(CLK1, 0);

  // hours
  digitalWrite(OUT0, any_leds_ & 0x010000);
  digitalWrite(OUT1, any_leds_ & 0x020000);
  digitalWrite(OUT2, any_leds_ & 0x040000);
  digitalWrite(OUT3, any_leds_ & 0x080000);
  digitalWrite(OUT4, any_leds_ & 0x100000);
  digitalWrite(OUT5, any_leds_ & 0x200000);
  digitalWrite(CLK2, 1);
  digitalWrite(CLK2, 0);
}

//-----------------------------------------------------------------------------
void resetTxBuf() {
  for (int i = 0; i < TXBUF_LEN; i++)
    tx_buf_[i] = 0;
}

//-----------------------------------------------------------------------------
void resetRxBuf() {
  for (int i = 0; i < RXBUF_LEN; i++)
    rx_buf_[i] = 0;
}

//-----------------------------------------------------------------------------
void listenForCmd(uint32_t timeout) {
  uint32_t stop = millis() + timeout;
  activateRF24();
  while (millis() < stop) {
    delay(10);
  }
  deactivateRF24();
}

//-----------------------------------------------------------------------------
void rf24_isr() {
  bool tx_ok, tx_fail, rx_ready;
  radio.whatHappened(tx_ok, tx_fail, rx_ready);
  if (rx_ready || radio.available()) {
    radio.read(rx_buf_, RXBUF_LEN);
    switch (rx_buf_[0]) {

      case CMD_CODE_ACK:
      {
        PRINTD(F("ACK RECEIVED"));
        retransmit_ = false;
        break;
      }

      case CMD_CODE_TIME_SYN:
      {
        PRINTD(F("CMD_CODE_TIME_SYN"));
        memcpy(&tp_, &rx_buf_[1], sizeof(time_packet_t));
        h_ = tp_.h;
        m_ = tp_.m;
        s_ = tp_.s;
        PRINTD(tp_.h_syn);
        PRINTD(tp_.m_syn);
        PRINTD(tp_.s_syn);
        time_updated_ = true;
        setLedsCb_ = setLeds;
        break;
      }

      case CMD_CODE_LED_TOGGLE:
      {
        PRINTD(F("CMD_CODE_LED_TOGGLE"));
        state_change_timeout_ = rx_buf_[1];
        leds_toggle_state_ = !leds_toggle_state_;
        setLedsCb_ = ledsToggle;
        break;
      }

      case CMD_CODE_PA_CHANGE:
      {
        PRINTD(F("CMD_CODE_PA_CHANGE"));
        current_pa_ = (rf24_pa_dbm_e)rx_buf_[1];
        radio.setPALevel(current_pa_);
        break;
      }

      case CMD_CODE_CHANNEL_CHANGE:
      {
        PRINTD(F("CMD_CODE_CHANNEL_CHANGE"));
        current_channel_ = rx_buf_[1];
        PRINTD(current_channel_);
        radio.setChannel(current_channel_);
        break;
      }

      case CMD_CODE_LIGHT_SENS:
      {
        PRINTD(F("CMD_CODE_LIGHT_SENS"));
        state_change_timeout_ = rx_buf_[1];
        setLedsCb_ = ledsLight;
        break;
      }

      case CMD_CODE_TEMP_SENS:
      {
        PRINTD(F("CMD_CODE_TEMP_SENS"));
        PRINTD(rx_buf_[1]);
        state_change_timeout_ = rx_buf_[1];
        setLedsCb_ = ledsTemperature;
        break;
      }

      case CMD_CODE_SET_LEDS:
      {
        PRINTD(F("CMD_CODE_SET_LEDS"));
        state_change_timeout_ = rx_buf_[1];
        any_leds_ = 0;
        any_leds_ |= (((uint32_t)rx_buf_[1]) << 16) & 0x00ff0000;
        any_leds_ |= (((uint32_t)rx_buf_[2]) <<  8) & 0x0000ff00;
        any_leds_ |= (((uint32_t)rx_buf_[3])      ) & 0x000000ff;
        PRINTD(any_leds_, HEX);
        setLedsCb_ = ledsAny;
        break;
      }

      case CMD_CODE_TIME:
      {
        PRINTD(F("CMD_CODE_TIME"));
        setLedsCb_ = setLeds;
        break;
      }

      case CMD_CODE_CALIB_TEMP:
      {
        PRINTD(F("CMD_CODE_CALIB_TEMP"));
        calib_data_.calib_temperature_int = analogRead(SENSOR_TEMP);
        calib_data_.calib_temperature_mdeg = (uint16_t)((uint16_t)rx_buf_[1]*256 + rx_buf_[2]);
        update_calib_data_ = true;
        PRINTD(calib_data_.calib_temperature_mdeg);
        PRINTD(calib_data_.calib_temperature_int);
        break;
      }

      case CMD_CODE_CALIB_LIGHT:
      {
        PRINTD(F("CMD_CODE_CALIB_LIGHT"));
        calib_data_.leds_off_threshold_percent = rx_buf_[1];
        update_calib_data_ = true;
        PRINTD(calib_data_.leds_off_threshold_percent);
        break;
      }

      default:
        PRINTD(F("unknown command"));
        PRINTD(rx_buf_[0], HEX);
    }
    resetRxBuf();
  }
  if (tx_fail) {
    retransmit_ = true;
  }
  if (tx_ok) {
    retransmit_ = false;
  }
}

//-----------------------------------------------------------------------------
void activateRF24() {
  if (radioIsOn_)
    return;
  bool initialized = radio.begin();
  if(initialized && radio.isChipConnected()) {
    radio.openWritingPipe(address_w[0]);
    radio.openReadingPipe(1,address_r[0]);
    radio.setAutoAck(true);
    radio.setRetries(15, 15);
    radio.setChannel(current_channel_);
    radio.setDataRate(RF24_250KBPS);
    radio.setCRCLength(RF24_CRC_16);
    radio.setPALevel(current_pa_);
    radio.startListening();
    // only interrupt on rx
    radio.maskIRQ(0, 0, 0);
    radioIsOn_ = true;
    attachInterrupt(digitalPinToInterrupt(RF24_IRQ_PIN), rf24_isr, LOW);
  } else {
    digitalWrite(ERRLED, HIGH);
    radioIsOn_ = false;
  }
}

//-----------------------------------------------------------------------------
void deactivateRF24() {
  if (!radioIsOn_)
    return;
  detachInterrupt(digitalPinToInterrupt(RF24_IRQ_PIN));
  radio.powerDown();
  SPI.end();
  radioIsOn_ = false;
}

//-----------------------------------------------------------------------------
bool initializeRadio() {
  radioIsOn_ = false;
  memset(&tp_, 0, sizeof(time_packet_t));
  memset(&mp_, 0, sizeof(measurements_packet_t));
  activateRF24();
  if (!radioIsOn_) {
    return false;
  }
  return true;
}

//-----------------------------------------------------------------------------
void sendMeasurements() {
  PRINTD(F("sendMeasurements()"));
  mp_.light = getLightPercent();
  mp_.temperature = getTemperatureMilliCelsius();
  resetTxBuf();
  tx_buf_[0] = PERIODIC_SENSOR_MEAS;
  memcpy(&tx_buf_[1], &mp_, sizeof(measurements_packet_t));
  PRINTD(mp_.sequence_number);
  PRINTD(mp_.light);
  PRINTD(mp_.temperature);
  send(3, 333);
  // always increase the sequence number. it tells the receiver how many
  // packets were lost
  mp_.sequence_number++;
}

//-----------------------------------------------------------------------------
void updateEeprom() {
  if (!update_calib_data_)
    return;

  EEPROM.put(SENSOR_CALIB_EEPROM_ADDR, calib_data_);
  update_calib_data_ = false;
}

//-----------------------------------------------------------------------------
bool send(uint8_t max_retries, uint32_t retry_delay) {
  digitalWrite(ERRLED, HIGH);
  wdt_enable(WDTO_1S);
  activateRF24();
  wdt_reset();
  // set retransmit_ to 'true'. It will be reset to false in rf24_isr()
  // if the transmission was successful
  retransmit_ = true;
  while (retransmit_ && max_retries != 0) {
    radio.stopListening();
    radio.write(tx_buf_, TXBUF_LEN);
    radio.startListening();
    wdt_reset();
    delay(retry_delay);
    max_retries--;
  }
  deactivateRF24();
  wdt_disable();
  if (retransmit_ && max_retries == 0) {
    PRINTD(F("max retries reached"));
    return false;
  } else {
    digitalWrite(ERRLED, LOW);
    PRINTD(F("tx ok"));
    return true;
  }
}

//-----------------------------------------------------------------------------
void syncTime() {
  // remember current time
  uint8_t tmp_h = h_;
  uint8_t tmp_m = m_;
  uint8_t tmp_s = s_;
  time_updated_ = false;
  setLedsCb_ = setLeds;
  // switch off leds
  h_ = 0;
  m_ = 0;
  s_ = 0;
  setLedsCb_();
  PRINTD(F("requesting time update"));
  resetTxBuf();
  resetRxBuf();
  tx_buf_[0] = REQ_CODE_TIME_SYN;
  send(10, 500);
  if (!time_updated_) {
    h_ = tmp_h;
    m_ = tmp_m;
    s_ = tmp_s; // maybe correct for sync delay? ~2 seconds
  }
}
